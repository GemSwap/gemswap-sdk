# GemSwap SDK

```sh
git clone https://github.com/gemswapfinance/gemswap-sdk.git
```

Move into the gemswap-sdk working directory

```sh
cd gemswap-sdk/
```

Install dependencies

```sh
yarn install
```
